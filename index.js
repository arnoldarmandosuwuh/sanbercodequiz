/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import FComponent from './src/screens/Quiz1/Soal1'
import ContextAPI from './src/screens/Quiz1/Soal2'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ContextAPI);
